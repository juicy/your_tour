$(document).ready(() ->
  radio '.b_radio'
  select '.b_select'
  datepicker '.m_datepicker'
  stick ['aside']
)

stick = (boxs) -> 
  $(boxs).each ->
    $('' + this).stickyfloat({duration: 0})

datepicker = (boxs) ->
  $(boxs).pickadate()

radio = (boxs) ->
  $(boxs).each ->
    box = this
    $(box).find('input:checked').each ->
      $(this).parent().addClass('active')
    $(box).find('input').change ->
      $(box).find('label').removeClass('active')
      $(this).parent().addClass('active')

select = (box) ->
  $(box + ' select').customSelect({customClass: "b_select_box"})

slider = (sliders) ->
  $(sliders).each ->
    input = this
    $('<span>').addClass("output").insertAfter($(this))
    $('<span class="minmax min">min</span>').insertAfter($(this))
    $('<span class="minmax max">max</span>').insertAfter($(this))
  $(sliders).bind("slider:ready slider:changed", (e, d) ->
    $(this).nextAll(".output:first").html("<i>до</i> " + d.value.toFixed(0) + " грн.")
  )

slider '[data-slider]'